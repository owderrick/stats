/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material.
 *
 *****************************************************************************/
/**
 * @file <stats.h>
 * @brief <Header files containing definitions and function declarations>
 *
 *
 * @author <Derrick>
 * @date <09/11/2017>
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

#define SIZE 40

// global variables for print_statisstics()
int max,min;
double mean, median;

/* Add Your Declarations and Function Comments here */

/**
 * @brief <Add Brief Description of Function Here>
 *
 * <Add Extended Description Here>
 *
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 *
 * @return <Add Return Informaiton here>
 */

/**
 * @brief <Prints the Statistics>
 *
 * <A function that prints the statistics of an array including minimum, maximum, mean(to two decimal places), and median(to two decimal places)>
 *
 */
void print_statistics();


/**
 * @brief <Prints the array>
 *
 * <Given an array of data and a length, prints the array to the screen>
 *
 * @param <unsigned int *arr> <given array>
 * @param <int length> <length of the given array>
 *
 */

void print_array(unsigned int *arr, int length);

/**
 * @brief <Calculates the mean value>
 *
 * <Given an array of data and a length, returns the mean>
 *
 * @param <unsigned int arr[]> <given array>
 * @param <int length> <length of the given array>
 *
 * @return <Returns the mean value of the given array>
 */
double find_mean(unsigned int *arr, int length);

/**
 * @brief <Calculates the median value>
 *
 * <Given an array of data and a length, returns the median>
 *
 * @param <unsigned int arr[]> <given array>
 * @param <int length> <length of the given array>
 *
 * @return <Returns the median of the given array>
 */
double find_median(unsigned int *arr, int length); //

/**
 * @brief <Find the maximum value>
 *
 * <Given an array of data and a length, returns the maximum>
 *
 * @param <unsigned int arr[]> <given array>
 * @param <int length> <length of the given array>
 *
 * @return <Returns the maximum value of the given array>
 */
int find_maximum(unsigned int *arr, int length);

/**
 * @brief <Find the minimum value>
 * <Given an array of data and a length, returns the minimum>
 *
 * @param <unsigned int arr[]> <given array>
 * @param <int length> <length of the given array>
 *
 * @return <Returns the minimum value of the given array>
 */
int find_minimum(unsigned int *arr, int length);

/**
 * @brief <Sorts array from least to greatest using the bubblesort algorithm>
 *
 * <Given an array of data and a length, sorts the array from smallest to largest using the bubblesort algorithm. (The zeroth Element should be the smallest value, and the last element (n-1) should be the largest value.)>
 *
 * @param <unsigned char arr []> <given array>
 * @param <int length> <length of the given array>
 *
 */
void bubble_sort(unsigned int *arr, int length);

#endif /* __STATS_H__ */
