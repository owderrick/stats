# README #

### What is this repository for? ###

This is the first assignment from Coursera's online course on "Introduction to Embedded Systems Software and 
Development Environments" by University of Colorado Boulder. It is a simple C program to sort 80 numbers from
smallest to largest and find the maximum, minimum, and median, and print the results on the screen.