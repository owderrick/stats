/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material.
 *
 *****************************************************************************/
/**
 * @file <stats.c>
 * @brief <C file to execute>
 *
 * <Calculates and prints the statistics of a given array>
 *
 * @author <Derrick>
 * @date <09/11/2017>
 *
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "stats.h"

int main() {

  unsigned int arr[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  printf("Unsorted: ");
  print_array(arr, SIZE);

  mean = find_mean(arr, SIZE);
  max = find_maximum(arr, SIZE);
  min = find_minimum(arr, SIZE);
  median = find_median(arr, SIZE);

  printf("\nSorted: ");
  print_array(arr, SIZE);
  print_statistics();

  return 0;
}

  /* Statistics and Printing Functions Go Here */
  void print_statistics(){

    printf("\nMean: %.2lf\n", mean);
    printf("\nMedian: %.2lf\n", median);
    printf("\nMaximum: %d\n", max);
    printf("\nMinimum: %d\n\n", min);
  }

  void print_array(unsigned int *arr, int length){

    int i;

    printf("[");

    for (i = 0; i < length; i++){
    	printf("%d", arr[i]);

    	if (i < length-1){
          printf(", ");
      }
      else{
        printf("]\n");
      }

    }

  }

  double find_mean(unsigned int *arr, int length){

    int i;
    int sum = 0;
    double mean = 0;

    for (i = 0; i < length; i++){
      sum += arr[i];
    }

    mean = (double)sum/length;
    mean = ceil(mean*100.0)/100.0;

    return mean;

  }

  double find_median(unsigned int *arr, int length){

    double median;

    bubble_sort(arr, length);

    // since length is 40(even), take average of middle two elements in array
    median = (double)(arr[length/2] + arr[length/2 - 1])/2;
    median = ceil(median*100.0)/100.0;

    return median;
  }

  int find_maximum(unsigned int *arr, int length){

    int i;
    int max = *arr; // set max equal to the first element of array arr

    for(i = 1; i < length; i++){
      if (arr[i]>max){
        max = arr[i];
      }
    }

    return max;

  }

  int find_minimum(unsigned int *arr, int length){

    int i;
    int min = *arr; // set max equal to the first element of array arr

    for(i = 1; i < length; i++){
      if (arr[i]<min){
        min = arr[i];
      }
    }

    return min;
  }

  void bubble_sort(unsigned int *arr, int length){

    bool is_sorted;
    int i,j,temp;

    for (i = 0; i < length; i++){
      is_sorted = true;

      for(j = 0; j < length; j++){
        if (arr[j] > arr[j+1]){
          temp = arr[j];
          arr[j] = arr[j+1];
          arr[j+1] = temp;
          is_sorted = false;
        }
      }

      if (is_sorted){
        break;
      }

    }

  }
